$(document).ready(function(){
	if (typeof(results) != "undefined")
	{
		for (var i = 0; i < results.length; i++)
		{
			var result = results[i]
			if (isNewUrl(result.url))
				addToQueue(result, (i == (results.length - 1)))
		}
  }

  var oTable = $("#compareGrid").dataTable({
    "bPaginate": false,
    "sDom": 'RC<"clear">lfrtip'
  });
  var oFH = new FixedHeader( oTable, { "top": true, "left": false, "right": false } );
  oFH.fnUpdate();

  $("#compareGrid > thead > tr").on("click", ".btnRemoveColumn", function(event) {
  	oTable.fnSetColumnVis( $(this).closest("th").index(), false)
  });

  $("#compareGrid > tbody > tr").on("click", ".btnRemoveRow", function(event) {
    $(this).closest("tr").detach()
  });

  $("#btnSearch").on("click", function(event) {
    var criteria = $("#inputCriteria").val()
    if (criteria.substring(0, 7) === "http://")
    {
			$("#btnCancel").click();
    	addUrl(criteria)
    }
    else
    {
			$('#loading-indicator').show();
			$("#searchResults tr").remove();
			$.get("/searchJson", {crit: criteria}, function(resp) {
				$.each(resp, function(key, result) {
					$("#searchResults tbody").append('<tr class="searchHit"><td><input type="checkbox" value=""></td><td><a href="' + result.url + '" target="_blank">' + result.productName + '</a> - ' + result.price + ' (' + result.store + ') </td></tr>');
				});
        $('#loading-indicator').hide();
			});
		}
  });

	$("#inputCriteria").keyup(function(event){
		if(event.keyCode == 13){
			$("#btnSearch").click();
		}
	});

  $("#btnAddAll").on("click", function(event) {
		$.each($("#searchResults tbody tr td a"), function(index, elem) {
			addUrl(elem.href)
		});
  });

  $("#btnAddSelected").on("click", function(event) {
		$.each($("#searchResults tbody tr"), function(index, elem) {
			// TODO: improve this to use some kind of find or id logic so we're not sensitive to changes in the table format
			if (this.firstChild.firstChild.checked)
				addUrl(this.children[1].firstChild.href)
		});
  });

	$(document).on('click','.dropdown ul a',function(){
			var text = $(this).text();
			$(this).closest('.dropdown').children('a.dropdown-toggle').text(text);
	});

	function addToQueue(result, isLastCol) {
		jQuery.ajaxQueue({
			type: "POST",
			url: "/getData",
			data: JSON.stringify(result),
			contentType : 'application/json',
			dataType: "json"
		}).done(function( specs ) {
			addColumn(specs)
			if (isLastCol)
				showDiffs();
		}).fail(function( msg ) {
			console.log(msg)
    });
	}

	function isNewUrl(url)  {
  	return ($("#compareGrid > thead > tr > th > a[href^='" + url + "']").length == 0)
  }

	function addUrl(url) {
		$.get("/dataJsonForUrl", {url: url}, function(specs) {
      //TODO: differentiate between orig URL and affiliated one
      if (isNewUrl(url))
			 addColumn(specs)
		});
	}

  var K_Url = " 0__URL"
  var K_Name = " 1__name"
  var K_PhotoUrl = " 2__photoUrl"
  var K_Price = " 3__price"
  var K_Store = " 4__store"

	function cleanKey(key) {
		return key.replace(/[ ()\/#.!]/g, "_")
	}

	function fixKey(id, key) {
		var keyHtml
		if (key === K_Url || key === K_PhotoUrl)
			keyHtml = ""
		else if (key === K_Name)
			keyHtml = "Product"
		else if (key === K_Price)
			keyHtml = "Price"
		else if (key === K_Store)
			keyHtml = "Store"
		else
			keyHtml = key

		return '<td id="' + id + '" class="keyColumn"><button class="btnRemoveRow">&times;</button><span class="keyName">' + keyHtml + '</span></td>'
	}

	function fixValue(key, val, numCols, url) {
		var valHtml
		if (key === K_Url)
			valHtml = '<a href="' + val + '"  target="_blank"> See in Store </a>&nbsp;&nbsp;'
		else if (key === K_PhotoUrl)
			valHtml = '<a href="' + url + '"> <img src="' + val + '" class="img-product"/></a>'
		else
			valHtml = val

		if (key === K_Name)
			return '<th id="col_' + (numCols-1) + '"><button class="btnRemoveColumn">&times;</button><a href="' + url + '">' + valHtml + '</th></a>'
		else
			return '<td>' + valHtml + '</td>'
	}

	function showDiffs() {
  	$("#compareGrid tbody tr").each(function(index) {
  		var key = $(this).children("td:nth-child(1)").text().trim()
  		if (key === "" || key === "Product" || key === "Store")
  			return true

  		var firstNonBlankValue = null
  		for (var i = 2; i <= $(this).children.length; i++)
  		{
  			var thisVal = $(this).children("td:nth-child(" + i + ")").text()
  			if (thisVal != "")
  			{
  				firstNonBlankValue = thisVal
  				break;
  			}
  		}

  		var thisRow = $(this)
      thisRow.children("td:gt(0)").each(function(index) {
				$(this).removeClass("diffValue")
				var val = $(this).text().trim()
        if (val != "" && val != firstNonBlankValue)
        {
          thisRow.children("td:gt(0)").each(function(index) {
          	if ($(this).text().trim() != "")
            	$(this).addClass("diffValue")
          })
					return false
        }
      })
  	})
	}

	function addColumn(specs) {
		var numCols = $("#compareGrid thead th").length
    var url = specs[K_Url]

		$.each(specs, function(key, val) {
			if (key == K_Url)
				return;

      var valHtml = fixValue(key, val, numCols, url)

			// if new key, insert a row
			var id = "key_" + cleanKey(key)
      if ($("#" + id).length <= 0)
      {
				var keyHtml = fixKey(id, key)
				var colFill = ""
				for (var i = 0; i < numCols - 1; i++)
					colFill += "<td></td>"

        var inserted = false
				$("#compareGrid tbody tr:gt(2)").each(function(ndx, row) {
					if ($(this).find(".keyName").text() > key)
					{
						var newRow = row.parentNode.insertRow(ndx + 2 + 1)
  	    		newRow.innerHTML = '<tr>' + keyHtml + colFill + '</tr>'
  	    		inserted = true
  	    		return false
  	    	}
  	    });

  	    if (!inserted)
  	    	$("#compareGrid tbody").append('<tr>' + keyHtml + colFill + '</tr>')
      }

			// add this new cell value
			$("#" + id).parent().append(valHtml);
		});

		// add a cell for any missing specs
		$("#compareGrid tbody tr" ).each(function(ndx, row) {
			if (row.children.length <= numCols)
				row.insertCell(-1)
		});

		// scroll to new column
		if (document.body.scrollTop == 0)
		{
			$('html,body').animate({
					scrollLeft: $("#compareGrid thead th:last-child").offset().left},
					'slow');
		}
    oFH.fnUpdate();
	}
});

