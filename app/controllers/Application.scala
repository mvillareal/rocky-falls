package controllers

import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import play.api.libs.json._
import models.{Scraper,Searcher,SearchResult}
import models.stores.AmazonSearchResult
import scala.collection.immutable.TreeSet

object Application extends Controller {
  def index = Action {
    Ok(views.html.index(filledUrlEntryForm)(filledSearchForm))
  }

  def addUrls = Action { implicit request =>
    val filledForm = urlEntryForm.bindFromRequest
    val results = for (url <- filledForm.get) yield new SearchResult(url, "", "", "")
    display(results)
  }

  def search = Action { implicit request =>
    val values = searchForm.bindFromRequest.data
    val criteria = values("criteria")
    val urls = Searcher.search(criteria, 5)
    display(urls)
  }

  def getData = Action(parse.json) { request =>
      (request.body \ "ASIN").asOpt[String].map { asin =>
        Ok(Json.toJson(Scraper.data(new AmazonSearchResult(asin, (request.body \ "url").as[String], "", "", ""))))
      }.getOrElse {
        (request.body \ "url").asOpt[String].map { url =>
          Ok(Json.toJson(Scraper.data(new SearchResult(url, "", "", ""))))
        }.getOrElse {
          BadRequest("Missing parameter [url]")
        }
      }
  }

  def display(results:List[SearchResult]) = {
    if (results.length > 0)
    {
      Ok(views.html.display(Scraper.standardKeys, results.map(_.serialize).mkString("[", ",", "]")))
    }
    else
    {
      Ok("Select at least 1 product.  Hit back.")
    }
  }

  def oldDisplay(results:List[SearchResult]) = {
    if (results.length > 0)
    {
      val data = for {
        result <- results
        if (result.url.length > 0)
      } yield Scraper.data(result)
      val myOrdering = Ordering.fromLessThan[String](_ < _)
      val keys = data.foldLeft(TreeSet.empty(myOrdering))(_ ++ _.keys)
      val combinedData = for {
        k <- keys.toList
      } yield (k, data.map(_.getOrElse(k, "")).toArray)
      Ok(views.html.oldDisplay(combinedData))
    }
    else
    {
      Ok("Select at least 1 product.  Hit back.")
    }
  }

  def searchJson(criteria:String) = Action { implicit request =>
    Ok(Json.toJson(Searcher.search(criteria, 5)))
  }

  def dataJson(url:String) = Action { implicit request =>
    Ok(Json.toJson(Scraper.data(new SearchResult(url, "", "", ""))))
  }

  val urlEntryForm = Form(
      "urls" -> list(text)
  )
  val filledUrlEntryForm = urlEntryForm.fill(List("http://www.homedepot.com/p/LG-Electronics-30-5-cu-ft-French-Door-Refrigerator-in-Stainless-Steel-Door-In-Door-Design-LFX31945ST/203295339?N=21jZ5yc1v#.UZvN9LXVDxo39:31",
    "http://www.lowes.com/pd_171409-2251-LFTR1814LW_0__?productId=3240643&Ntt=refrigerator&pl=1&currentURL=%3FNtt%3Drefrigerator&facetInfo="))

  val searchForm = Form(
    tuple(
      "store" -> text,
      "criteria" -> text
    )
  )

  val filledSearchForm = searchForm.fill("Home Depot", "GE french door refrigerator")
}
