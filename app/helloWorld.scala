import play.api.libs.json._

abstract class Animal (val name:String) {
  def serialize = "Animal"
}

case class Dog (override val name:String, val breed: String)
  extends Animal(name)  {
  implicit val writes = Json.writes[Dog]
  override def serialize = Json.toJson(this).toString
}

case class Cat (override val name:String, val hairLength: Int)
  extends Animal(name)  {
  implicit val writes = Json.writes[Cat]
  override def serialize = Json.toJson(this).toString
}

object helloWorld extends App {
  //  The list below outputs:     [{"name":"Ruff","breed":"labrador"}]
  //  val l = List[Dog](Dog("Ruff", "labrador"))
    //val l = List[Animal](Dog("Ruff", "labrador"), Dog("Fido", "beagle"))
  //val l = List[Dog](Dog("Ruff", "labrador"), Dog("Fido", "beagle"))
  //  The list below outputs:     [{"name":"Ruff"},{"name":"Fluffy"}]
  //  I expect to see: [{"name":"Ruff","breed":"labrador"},{"name":"Fluffy","hairLength":3}]
  val l = List[Animal](Dog("Ruff", "labrador"), Cat("Fluffy", 3))
  //println(Json.toJson(l))
  println((l.map(_.serialize).mkString("[", ",", "]")))

}