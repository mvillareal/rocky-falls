package models

import scala.xml.NodeSeq
import models.stores._

object whatDiff extends App {
  //val page = "http://www.homedepot.com/p/LG-Electronics-30-5-cu-ft-French-Door-Refrigerator-in-Stainless-Steel-Door-In-Door-Design-LFX31945ST/203295339?N=21jZ5yc1v#.UZvN9LXVDxo39:31"
  val page = "http://www.lowes.com/pd_364023-46-GZ25FSRXYY_4294728374%2B4294857973__?productId=3478135&Ns=p_product_qty_sales_dollar|1&pl=1&currentURL=%3FNs%3Dp_product_qty_sales_dollar%7C1&facetInfo="
  val data = Scraper.data(new SearchResult(page, "", "", ""))
  data.foreach(println)
}

object Scraper
{
  val K_Url = " 0__URL"
  val K_Name = " 1__name"
  val K_PhotoUrl = " 2__photoUrl"
  val K_Price = " 3__price"
  val K_Store = " 4__store"
  val K_Error = " 999__error"

  val standardKeys = List(K_Url, K_Name, K_PhotoUrl, K_Price, K_Store)

  val normalizedKeys = Map(
    "Cabinet Color" -> "Color - Appliance Cabinet",
    "CEE Tier Qualified" -> "Energy Efficiency Tier Rating",
    "Depth (Excluding Handles)" -> "Depth (Excluding Handles) (Inches)",
    "Depth Without Handle" -> "Depth (Excluding Handles) (Inches)",
    "Depth w/o Handle (in.)" -> "Depth (Excluding Handles) (Inches)",
    "Depth (Including Handles)" -> "Depth (Including Handles) (Inches)",
    "Depth w/ Handle (in.)" -> "Depth (Including Handles) (Inches)",
    "Depth (Less Door)" -> "Depth (Less Door) (Inches)",
    "Depth without Door (in.)" -> "Depth (Less Door) (Inches)",
    "Depth With Door Open 90 Degrees (In)" -> "Depth with Door Open (Inches)",
    "Depth w/ Door Open 90 Degrees" -> "Depth with Door Open (Inches)",
    "Depth with Door Open 90 Degrees" -> "Depth with Door Open (Inches)",
    "Dual Evaporated Cooling System" -> "Dual Evaporator Cooling System",
    "ENERGY STAR Qualified" -> "Energy Star Compliant",
    "ENERGY STAR Compliant" -> "Energy Star Compliant",
    "Freezer Capacity (Cu. Feet)" -> "Capacity (cu. Ft.) - Freezer",
    "Freezer Capacity" -> "Capacity (cu. Ft.) - Freezer",
    "Freezer Door Type" -> "Freezer Door Style",
    "Height to Top of Door Hinge" -> "Height to Top of Door Hinge (Inches)",
    "Height to Top of Hinge (in.)" -> "Height to Top of Door Hinge (Inches)",
    "Height to Top of Refrigerator (in.)" -> "Height to Top of Case (Inches)",
    "Height to Top of Case (in.)" -> "Height to Top of Case (Inches)",
    "Height to Top of Case" -> "Height to Top of Case (Inches)",
    "Hidden Hinge" -> "Hidden Door Hinges",
    "Hidden Door Hinge(s)" -> "Hidden Door Hinges",
    "Humidity Controlled Crisper" -> "Humidity-Controlled Crispers",
    "Number of Freezer Shelves" -> "Number of Freezer Shelves/Baskets",
    "Overall Capacity (Cu. Feet)" -> "Capacity (cu. Ft.) - Total",
    "Overall Capacity (Cu Ft)" -> "Capacity (cu. Ft.) - Total",
    "Overall Depth" -> "Product Depth",
    "Overall Height" -> "Product Height",
    "Overall Width" -> "Product Width",
    "Refrigerator Capacity (Cu. Feet)" -> "Capacity (cu. Ft.) - Fresh Food",
    "Refrigerator Capacity" -> "Capacity (cu. Ft.) - Fresh Food",
    "Refrigerator Width (In.)" -> "Width (Inches)",
    "Safety Lock" -> "Child Safety Locks",
    "Total Capacity" -> "Capacity (cu. Ft.) - Total",
    "Volts" -> "Voltage (V)",
    "Water Filter Indicator" -> "Water-Filtration Replacement Indicator Light"
  )

  def data(res:SearchResult) =
  {
    try
    {
      res.url match {
        case url if url.startsWith("http://www.homedepot.com") => HomeDepot.data(res)
        case url if url.startsWith("http://www.amazon.com") => Amazon.data(res)
        case url if url.startsWith("http://www.lowes.com") => Lowes.data(res)
        case url if url.startsWith("http://www.sears.com") => Sears.data(res)
        case url if url.startsWith("http://www.ajmadison.com") => AJMadison.data(res)
        case url => Map("url" -> url,  "Error" -> "Unknown Store")//HomeDepot.data(res)
      }
    } catch {
      case unknown : Exception => {
        System.out.println("[error] " + res.url + ". " + unknown )
        Map("url" -> res.url,  K_Error -> unknown.getMessage())
      }
    }
  }
}

class Scraper
{
  def cleanText(s:String) : String  =
    s.trim.replace("\u00a0","")

  def cleanText(n:scala.xml.Node) : String  =
    cleanText(n.text)

  def cleanKey(s:String): String = {
    var key = cleanText(s)
    if (key.endsWith(":"))
      key = key.init
    Scraper.normalizedKeys.getOrElse(key, key)
  }

  def cleanKey(n:scala.xml.Node): String =
    cleanKey(n.text)

  def cleanText(ns:scala.xml.NodeSeq) : String =
    if (ns.length > 0)
      cleanText(ns(0))
    else
      ""

  def specData(specs: NodeSeq) = {
    val specList = for {
      spec <- specs.toList
      if (spec \ "td").length >= 2 && cleanKey((spec \ "td")(0)).length > 0
    } yield (cleanKey((spec \ "td")(0)) , cleanText((spec \ "td")(1)))

    val specList2 = for {
      spec <- specs.toList
      if (spec \ "td").length >= 4 && cleanKey((spec \ "td")(2)).length > 0
    } yield (cleanKey((spec \ "td")(2)) , cleanText((spec \ "td")(3)))

    Map(specList: _*) ++ Map(specList2: _*)
  }
}