package models

import scala.xml.{Source, Elem, XML}
import scala.xml.factory.XMLLoader

import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl

object TagSoupXmlLoader {
  private val factory = new SAXFactoryImpl()

  def get(): XMLLoader[Elem] = {
    XML.withSAXParser(factory.newSAXParser())
  }

  def urlToStream(url: String) = Source.fromInputStream(
    (new java.net.URL(url).openConnection match {
      case connection: java.net.HttpURLConnection => {
        connection.setInstanceFollowRedirects(true)
        connection
      }
      case connection => connection
    }).getInputStream
  )

  def loadWithRedirect(url: String) = {
    get().load(urlToStream(url))
  }
}
