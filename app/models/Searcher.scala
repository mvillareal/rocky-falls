package models

import models.stores._
import play.api.libs.json._

object SearchResult {
  implicit object SearchResultWrites extends Writes[SearchResult] {
    def writes(ts: SearchResult) = JsObject(Seq(
      "url" -> JsString(ts.url),
      "productName" -> JsString(ts.productName),
      "price" -> JsString(ts.price),
      "store" -> JsString(ts.store)))
  }
}

class SearchResult(val url: String, val productName: String, val price: String, val store: String) {
  def serialize = Json.toJson(this).toString
}

object Searcher {
  def search(criteria:String, max:Integer) : List[SearchResult] = {
    HomeDepot.search(criteria).take(max) ++
     // Lowes.search(criteria).take(max) ++
      //Sears.search(criteria).take(max) ++
      Amazon.search(criteria).take(max) //++
      //AJMadison.search(criteria).take(max)
  }
}
