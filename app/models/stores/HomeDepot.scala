package models.stores

import models.{Scraper, TagSoupXmlLoader}
import models.SearchResult

object HomeDepot extends Scraper {
  val STORE_NAME = "Home Depot"
  def data(res:SearchResult) : Map[String, String] = {
    val root = TagSoupXmlLoader.get().load(res.url)
    val name = cleanText(root \\ "h1" \ "span")
    val price = cleanText((root \\ "span").filter(n => (n \ "@itemprop").text == "price"))
    val photoSrc = cleanText((((root \\ "a" \ "img").filter(n => (n \ "@itemprop").text == "image")) \ "@src"))

    specData(root \\ "div" \ "table" \ "tbody" \ "tr") + (Scraper.K_Url -> res.url) + (Scraper.K_Name -> name) + (Scraper.K_PhotoUrl -> photoSrc)  + (Scraper.K_Price -> price) + (Scraper.K_Store -> STORE_NAME)
  }

  def search(criteria:String) : List[SearchResult] = {
    val url = "http://www.homedepot.com/webapp/catalog/servlet/Search?storeId=10051&langId=-1&catalogId=10053&Ns=None&Ntpr=1&Ntpc=1&selectedCatgry=Search+All&keyword=" + java.net.URLEncoder.encode(criteria, "UTF-8")
    val root = TagSoupXmlLoader.get().load(url)
    for {
      ns <- (root \\ "div").toList
      if ((ns \ "a" \ "@class").text == "item_description")
      n <- ns
    } yield(new SearchResult(n \ "a" \ "@href" text, // url
        n \ "a" text, // product name
        (n \ "div" \ "span").filter(n => (n \ "@class").text == "xlarge item_price") text, // price
        STORE_NAME
    ))
  }
}
