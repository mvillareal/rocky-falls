package models.stores

import models.{Scraper, TagSoupXmlLoader, SearchResult}

object Lowes extends Scraper {
  def data(res:SearchResult) : Map[String, String] = {
    val root = TagSoupXmlLoader.get().load(res.url)

    val name = cleanText(root \\ "h1")

    //price doesn't work!! it doesn't seem to be in root, try not using TagSoup
    val price = cleanText((root \\ "div").filter(n => (n \ "@id").text == "pricing") \ "span")

    val photoSrc = cleanText((((root \\ "a" \ "img").filter(n => (n \ "@id").text == "prodPrimaryImg")) \ "@src"))

    specData(root \\ "div" \ "table" \ "tr") + (Scraper.K_Url -> res.url) + (Scraper.K_Name -> name) + (Scraper.K_PhotoUrl -> photoSrc)  + (Scraper.K_Price -> price) + (Scraper.K_Store -> "Lowes")
  }

  //doesn't work!
  def search(criteria:String) : List[String] = {
    val url = "http://www.lowes.com/Search=?newSearch=true&Ntt=" + java.net.URLEncoder.encode(criteria, "UTF-8")
    val root = TagSoupXmlLoader.get().load(url)
    (root \\ "a").filter(n => (n \ "@name").text == "listpage_productname").map(n => "http://www.lowes.com" + (n \ "@href").text).toList
  }
}

