package models.stores

import models.{SearchResult, Scraper, TagSoupXmlLoader}
import co.orderly.scalapac.OperationHelper
import play.api.libs.json._

object AmazonSearchResult {
  implicit val searchResultsFormat = Json.format[AmazonSearchResult]
}

case class AmazonSearchResult(val ASIN: String, override val url: String, override val productName: String, override val price: String, override val store: String) extends
  SearchResult(url, productName, price, store) {
  override def serialize = Json.toJson(this).toString
}

object Amazon extends Scraper {
  val STORE_NAME = "Amazon"
  lazy val opHelper = new OperationHelper("AKIAIMZQLFQ7T2JTVZ3Q", "yO+JNyYJR4vhVUNBG+y4YweZ1sP6qcfr2787O2mx", "ariscris-20")

  def data(res:SearchResult) : Map[String, String] = {
    val root = TagSoupXmlLoader.get().load(res.url)
    val name = cleanText(root \\ "h1" \ "span")
    val price = cleanText((root \\ "span").filter(n => (n \ "@class").text == "price"))
    val photoSrc = cleanText((((root \\ "img").filter(n => (n \ "@id").text == "main-image")) \ "@src"))

    specData(root \\ "div" \ "table" \ "tbody" \ "tr") + (Scraper.K_Url -> res.url) + (Scraper.K_Name -> name) + (Scraper.K_PhotoUrl -> photoSrc)  + (Scraper.K_Price -> price) + (Scraper.K_Store -> STORE_NAME)
  }

  def data(res:AmazonSearchResult) : Map[String, String] = {
    val (ret, root) = opHelper.execute("ItemLookup",
      Map("ItemId" -> res.ASIN,
        "ResponseGroup" -> "ItemAttributes"
    ))

    val specList = for {
      attribs <- (root \ "Items" \ "Item" \ "ItemAttributes").toList
      attrib <- attribs.nonEmptyChildren
    } yield (cleanKey(attrib.label), cleanText(attrib.text))

    Map(specList: _*)
  }

  def search(criteria:String) : List[SearchResult] = {
    try { 
      val (ret, root) = opHelper.execute("ItemSearch",
        Map("SearchIndex" -> "Appliances",
         "Keywords"       -> criteria,
         "ResponseGroup"  -> "ItemAttributes"
      ))

      for {
        item <- (root \ "Items" \ "Item").toList
      } yield (new AmazonSearchResult( item \ "ASIN" text, //ASIN
        item \ "DetailPageURL" text,  //url
        item \ "ItemAttributes" \ "Title" text,  // product name
        item \ "ItemAttributes" \ "ListPrice" \ "FormattedPrice" text, // price
        STORE_NAME))
    } catch {
      case e: VerifyError => System.out.println("[error] Searching using Amazon API for " + criteria + ".  " + e.getMessage())
      List()
    }
  }
}
