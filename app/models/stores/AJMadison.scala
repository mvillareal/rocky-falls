package models.stores

import models.{TagSoupXmlLoader, Scraper}
import models.SearchResult

object AJMadison extends Scraper {
  val STORE_NAME = "AJ Madison"
  def data(res:SearchResult) : Map[String, String] = {
    val root = TagSoupXmlLoader.get().load(res.url)
    val name = cleanText(root \\ "h1")
    val price = cleanText((root \\ "span").filter(n => (n \ "@itemprop").text == "price"))
    val photoSrc = "http://ajmadison.com" + cleanText((((root \\ "a").filter(n => (n \ "@id").text == "Zoomer3")) \ "@href"))
    val specs = (root \\ "div").filter(n => (n \ "@id" text) == "propgTabContentSpecifications") \ "div" \ "table" \ "tr"
    specData(specs) + (Scraper.K_Url -> res.url) + (Scraper.K_Name -> name) + (Scraper.K_PhotoUrl -> photoSrc)  + (Scraper.K_Price -> price) + (Scraper.K_Store -> STORE_NAME)
  }

  // returns some kind of redirection page...
  def search(criteria:String) : List[SearchResult] = {
    val url = "http://www.ajmadison.com/b.php?Ntt=" + java.net.URLEncoder.encode(criteria, "UTF-8")
    val root = TagSoupXmlLoader.get().load(url)
//    ((root \\ "a").filter(n => (n \ "@itemprop" text) == "name")).map(n => "http://www.ajmadison.com" + (n \ "@href" text)).toList

    for {
      ns <- (root \\ "div").toList
      n <- ns
    } yield(new SearchResult("http://www.tkqlhce.com/click-7145599-10588758?url=" + java.net.URLEncoder.encode("http://www.ajmadison.com" + (n \ "div" \ "h2" \ "a" \ "@href" text), "UTF-8"), // url
        n \ "div" \ "h2" \ "a" text, // product name
        (n \\ "div" \ "span").filter(n => (n \ "@itemprop").text == "price") text, // price
        STORE_NAME
    ))

  }
}
