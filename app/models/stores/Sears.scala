package models.stores

import models.{TagSoupXmlLoader, Scraper}
import models.SearchResult

object Sears extends Scraper {
  val STORE_NAME = "Sears"
  def data(res:SearchResult) : Map[String, String] = {
    val root = TagSoupXmlLoader.get().load(res.url)
    val name = cleanText(root \\ "h1")
    val price = cleanText((root \\ "span").filter(n => (n \ "@itemprop").text == "price"))
    val photoSrc = cleanText((((root \\ "meta").filter(n => (n \ "@property").text == "og:image")) \ "@content"))

    specData(root \\ "div" \ "table" \ "tbody" \ "tr") + (Scraper.K_Url -> res.url) + (Scraper.K_Name -> name) + (Scraper.K_PhotoUrl -> photoSrc)  + (Scraper.K_Price -> price) + (Scraper.K_Store -> STORE_NAME)
  }

  def search(criteria:String) : List[SearchResult] = {
    val url = "http://www.sears.com/search=" + java.net.URLEncoder.encode(criteria, "UTF-8") + "?storeId=10153&catalogId=12605&vName=Appliances&viewItems=50&catPrediction=false&sLevel=0&redirectType=SKIP_LEVEL"
    val root = TagSoupXmlLoader.get().load(url)

    for {
      ns <- (root \\ "div").toList
      if ((ns \ "@class").text == "cardInner")
      n <- ns
    } yield(new SearchResult("http://www.sears.com" + (n \\ "div" \ "h4" \ "a" \ "@href" text), // url
        n \\ "div" \ "h4" \ "a" text, // product name
        (n \\ "div" \ "span").filter(n => (n \ "@class").text == "price_v2 intShipHide") text, // price
        STORE_NAME
    ))
  }
}
